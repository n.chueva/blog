---
title: "Junior frontend developer"
date: 2018-09-21T14:33:12+02:00
draft: false
---
<div class="row-modify">
    <div class="column-data">e-mail: nsbatikova@gmail.com</div>
    <div class="column-data">phone: +49 (1525) 8421574</div>
</div>

<div class="content-justf"><p>I have about 2 years of experience as a Frontend developer and good skills in design.
I continue to improve my skills in a field of Frontend development. Continuous and determined
learning allows me to stay focused and achieve my goals faster.</p></div>

<h2>Skills</h2>
<div class="row-modify">
    <div class="column">
        <div class="skills"><h3>Frontend development</h3></div>
        <div> HTML5, CSS3, JavaScript, jQuery, CMS WordPress, Bootstrap, Git, GitLab, Hugo </div>
    </div>
    <div class="column left-border ">
        <div class="skills"><h3>Design</h3></div>
        <div>Adobe Suite (Adobe Photoshop, Adobe Illustrator), Gimp, AutoCAD, photo retouching</div>
    </div>
</div>

<h2>Experience</h2>
<div class="row-modify left">
    <div class="column-experience first-exp">
      <h3>SMP Bank, Frontend developer <br><span class="text-size">June 2016 - July 2017 (Russian Federation)</span></h3>
      I worked in a team where we developed and supported websites of SMP Bank Group (SMP Bank, MOSOBLBANK, InvestCapitalBank). I used HTML and CSS for improvement of existing pages, creating new pages(also promo) and separate blocks (inkl. by the design-template from designers team). Of course we used Git. We always met our deadlines. I was also responsible for content management (adding news, banners, rates, etc.) and social networks.
    </div>
</div>
<div class="row-modify left">
    <div class="column-experience first-exp last-dot"> 
      <h3>SK 10-GPZ, Engineer <br><span class="text-size">April 2012 - December 2014 (Russian Federation)</span></h3>
     Construction and design. I took a part in development of design solutions (sections: production technology,
architecture), worked a lot with project documentations of various sections ( architecture, concrete structures,
master plan, production technology, etc) and normative literature in construction and design. I was engaged
into calculation of the scope of work, cost planning, inventory at construction sites, conclusion of contracts
(analysis, completion, signing) and monitoring its implementation. Most of works were in AutoCAD. Thanks to
my work we completed the projects on time and put the object into operation.
    </div>

</div>

<h2>Education</h2>
<div class="row-modify">Specialization: Industrial and civil engineering<br>
Rostov State University of Civil and Industrial Engineering <br> (Russian Federation)
</div>

<h2>Languages</h2>
<div class="row-modify">
    <div class="column-lang">Russian</div>
    <div class="column-lang">English</div>
    <div class="column-lang">German</div>
</div>
