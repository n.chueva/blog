---
title: "Examples of works"
date: 2018-10-17T11:36:16+02:00
draft: false
---

There are links with examples of my works and projects in wich I participated: 

- <a href="https://smpbank.ru/">SMP Bank.ru</a>. 

- <a href="https://mosoblbank.ru/">Mosoblbank.ru</a>. 

- <a href="http://alexandria161.ru/">alexandria161.ru</a>. I developed whole website. 

- <a href="http://bk.smpbank.ru/capabilities/">FAQ and etc.</a> Part of SMP Online-Banking for business. 
